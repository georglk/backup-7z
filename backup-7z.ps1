#!/usr/bin/env pwsh
Param (

	[Parameter(Mandatory=$true)]
	[ValidateNotNullOrEmpty()]
	[string]
	$src,

	[Parameter(Mandatory=$true)]
	[ValidateNotNullOrEmpty()]
	[string]
	$dst,

	[Parameter(Mandatory=$true)]
	[ValidateNotNullOrEmpty()]
	[Alias('arch')]
	[string]
	$name,

	[string]
	$prefix,

	[Alias('log')]
	[string]
	$logfile

)

#Set-StrictMode -Version 'Latest'

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$Script:ErrorActionPreference = [System.Management.Automation.ActionPreference]::Stop

$Script:total_errors = 0

# logging varitable
New-Variable -Name log -Force -ErrorAction SilentlyContinue

# for PowerShell 2.0
if (! (Get-Variable -Name PSCommandPath -ErrorAction SilentlyContinue)) {
	$PSCommandPath = $MyInvocation.MyCommand.Definition
}

# for PowerShell ISE
if (Get-Variable -Name psISE -ErrorAction SilentlyContinue) {
	# call function from $profile
	if (Get-Command -Name 'Remove-UserVariable' -ErrorAction SilentlyContinue) {
		Remove-UserVariable
	}
	ipconfig | Out-Null
	[Console]::OutputEncoding = [System.Text.Encoding]::GetEncoding("cp866")
}

function Write-ErrorObject {
<#
	.SYNOPSIS
		Return string from ErrorRecord object.
#>

	param ([PSObject] $InputObject)

	begin {
		$line = @()
	}

	process {
		$line += 'Error'

		if ($InputObject.InvocationInfo.MyCommand) {
			$line += $InputObject.InvocationInfo.MyCommand.ToString()
		}

		$line += ':'

		if ($InputObject.Exception.Message) {
			$line += $InputObject.Exception.Message.ToString().Trim()
		}

		if ($InputObject.InvocationInfo.ScriptLineNumber) {
			$line += 'line'
			$line += $InputObject.InvocationInfo.ScriptLineNumber.ToString()
		}
	}

	end {
		return ($line -join ' ')
	}
}

function Write-StdErr {
<#
	.SYNOPSIS
		Writes text to stderr when running in a regular console window,
		to the hosts error stream otherwise.

	.DESCRIPTION
		Writing to true stderr allows you to write a well-behaved CLI
		as a PS script that can be invoked from a batch file, for instance.

	Note that PS by default sends ALL its streams to *stdout* when invoked from cmd.exe.

	This function acts similarly to Write-Host in that it simply calls
	.ToString() on its input; to get the default output format, invoke
	it via a pipeline and precede with Out-String.
#>

	param ([PSObject] $InputObject)

	$outFunc = if ($Host.Name -eq 'ConsoleHost') {
		[Console]::Error.WriteLine
	} else {
		$Host.UI.WriteErrorLine
	}

	if ($InputObject) {
		[void] $outFunc.Invoke($InputObject.ToString())
	} else {
		[string[]] $lines = @()
		$Input | ForEach-Object { $lines += $_.ToString() }
		[void] $outFunc.Invoke($lines -join [Environment]::NewLine)
	}
}

function Join-Words {
	[CmdletBinding()]
	param (
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[string[]]
		$words,
		[Parameter(Position = 1)]
		[string]
		$Delimiter = " "
	)

	begin {
		$items = @()
	}

	process {
		$items += $words
	}

	end {
		return ($items -join $Delimiter)
	}
}

function Logging {
	[CmdletBinding()]
	param (
		[Parameter(Mandatory=$true,ValueFromPipeline=$true,Position = 0)]
		[string[]]
		$line,

		[Parameter(Position = 1)]
		[ref]
		$var,

		[switch]
		$stderr,

		[string]
		$file

	)

	if ($stderr) {
		$line | Write-StdErr
	} else {
		Write-Output $line
	}

	if ($var) {
		$OFS = [Environment]::NewLine
		$var.Value = $var.Value + $OFS + $line
	}

	if ($file) {
		try {
			$line | Out-File -LiteralPath $file -Append -Force
			# -Encoding
		} catch {
			Write-ErrorObject $_ | Logging -var $var -stderr
		}
	}

}

function Elapsed {
	[CmdletBinding()]
	param (
		[Parameter()]
		[switch]
		$stop
	)

	process {
		if ($stop) {
			$Script:watch.Stop()
			return $Script:watch.Elapsed.ToString()
		} else {
			$Script:watch = [System.Diagnostics.Stopwatch]::StartNew()
		}
	}

	end {
		if ($stop) {
			Remove-Variable -Name watch -Scope Script -Force
		}
	}
}

function Invoke-Process {
	Param(
		[Parameter(Mandatory=$true)]
		[String]
		$FileName,

		[String]
		$Arguments,

		[String]
		$Verb
	)

	$Result = [PSCustomObject]@{}
	$Result | Add-Member -MemberType NoteProperty -Name FileName -Value $FileName -Force
	$Result | Add-Member -MemberType NoteProperty -Name Arguments -Value $Arguments -Force

	Elapsed
	$Error.Clear()

	try {
		# https://stackoverflow.com/questions/24370814/how-to-capture-process-output-asynchronously-in-powershell/24371479#24371479
		# https://docs.microsoft.com/ru-ru/dotnet/api/system.diagnostics.processstartinfo
		$ProcessStartInfo = New-Object -TypeName System.Diagnostics.ProcessStartInfo
		$ProcessStartInfo.FileName = $FileName
		if (! [String]::IsNullOrEmpty($Arguments)) {
			$ProcessStartInfo.Arguments = $Arguments
		}
		if (! [String]::IsNullOrEmpty($Verb)) {
			$ProcessStartInfo.Verb = $Verb
		}
		$ProcessStartInfo.RedirectStandardError = $true
		$ProcessStartInfo.RedirectStandardOutput = $true
		$ProcessStartInfo.UseShellExecute = $false
		$ProcessStartInfo.CreateNoWindow = $true
		$ProcessStartInfo.WindowStyle = 'Hidden'
		# https://docs.microsoft.com/ru-ru/dotnet/api/system.diagnostics.process
		$Process = New-Object -TypeName System.Diagnostics.Process
		$Process.StartInfo = $ProcessStartInfo
		# Creating string builders to store stdout and stderr
		$StdOutBuilder = New-Object -TypeName System.Text.StringBuilder
		$StdErrBuilder = New-Object -TypeName System.Text.StringBuilder
		# Adding event handers for stdout and stderr
		$sScripBlock = {
			if (! [String]::IsNullOrEmpty($EventArgs.Data)) {
				$Event.MessageData.AppendLine($EventArgs.Data)
			}
		}
		$StdOutEvent = Register-ObjectEvent -InputObject $Process -Action $sScripBlock -EventName 'OutputDataReceived' -MessageData $StdOutBuilder
		$StdErrEvent = Register-ObjectEvent -InputObject $Process -Action $sScripBlock -EventName 'ErrorDataReceived' -MessageData $StdErrBuilder

		$Process.Start() | Out-Null
		$Process.BeginOutputReadLine()
		$Process.BeginErrorReadLine()
		$Process.WaitForExit()

		# Unregistering events to retrieve process output.
		Unregister-Event -SourceIdentifier $StdOutEvent.Name
		Unregister-Event -SourceIdentifier $StdErrEvent.Name

	}
	catch {
		# catch in execite process
		$Result | Add-Member -MemberType NoteProperty -Name Catch -Value $_ -Force
	}
	finally {
		$ExitCode = $Process.ExitCode
		$Process.Close()
	}

	$Result | Add-Member -MemberType NoteProperty -Name Elapsed -Value $(Elapsed -stop) -Force

	if ([string]::IsNullOrEmpty($ExitCode)) {
		# exit code not existing
		$Result | Add-Member -MemberType NoteProperty -Name ExitCode -Value $null -Force
	} else {
		# exit code existing
		$Result | Add-Member -MemberType NoteProperty -Name ExitCode -Value $ExitCode -Force
	}

	if (! [string]::IsNullOrEmpty($StdOutBuilder.ToString().Trim())) {
		$Result | Add-Member -MemberType NoteProperty -Name stdout -Value $StdOutBuilder.ToString().Trim() -Force
	}

	if (! [string]::IsNullOrEmpty($StdErrBuilder.ToString().Trim())) {
		$Result | Add-Member -MemberType NoteProperty -Name stderr -Value $StdErrBuilder.ToString().Trim() -Force
	}

	return $Result
}

function Join-Rows {
	param (
		[parameter(Mandatory = $true, Position = 0)]
		[ref]
		$var,

		[parameter(Mandatory = $true, Position = 1)]
		[string[]]
		$text,

		[Alias('n', 'nl')]
		[switch]
		$newline,

		[Alias('s', 'sp')]
		[switch]
		$space
	)

	$OFS = [Environment]::NewLine

	if ($newline) {
		if ($space) {
			$var.Value = $var.Value + $OFS + ' ' + "$text"
		} else {
			$var.Value = $var.Value + $OFS + "$text"
		}
	} else {
		if ($space) {
			$var.Value = $var.Value + ' ' + "$text"
		} else {
			$var.Value = $var.Value + "$text"
		}
	}
}

function Get-ExitCodeDescription7z {
	param ([int] $exitcode)

	if (! $exitcode) {
		return $null
	}

	# https://sourceforge.net/p/sevenzip/discussion/45797/thread/c374fd35/#f956/3d2c/d109
	switch ($exitcode) {
		0 {return 'Successful operation'}
		1 {return 'Non fatal error(s) occurred'}
		2 {return 'A fatal error occurred'}
		3 {return 'A CRC error occurred when unpacking'}
		4 {return 'Attempt to modify an archive previously locked'}
		5 {return 'Write to disk error'}
		6 {return 'Open file error'}
		7 {return 'Command line option error'}
		8 {return 'Not enough memory for operation'}
		9 {return 'Create file error'}
		255 {return 'User stopped the process'}
		Default {return $null}
	}
}

New-Variable -Name Text -Force -ErrorAction SilentlyContinue
Join-Rows ([ref]$Text) ([DateTime]::Now.ToString('u').Replace('Z',''))
Join-Rows ([ref]$Text) 'Start' -space
if ($PSCommandPath) {
	Join-Rows ([ref]$Text) $(Split-Path -Leaf -Path $PSCommandPath) -space
}
Join-Rows ([ref]$Text) '::' -space
Join-Rows ([ref]$Text) ([Environment]::MachineName) -space
Join-Rows ([ref]$Text) '@' -space
Join-Rows ([ref]$Text) ([Environment]::UserName) -space
$Text | Logging -var ([ref]$log) -file $logfile

# get path to 7z
if (Test-Path -PathType Leaf -Path $([IO.Path]::Combine('/usr/bin', '7z'))) {
	$exe = [IO.Path]::Combine('/usr/bin', '7z')
}
elseif (Test-Path -PathType Leaf -Path $([IO.Path]::Combine($env:ProgramFiles, '7-Zip', '7z.exe'))) {
	$exe = [IO.Path]::Combine($env:ProgramFiles, '7-Zip', '7z.exe')
}
elseif (Test-Path -PathType Leaf -Path $([IO.Path]::Combine($PSCommandPath, '7z.exe'))) {
	$exe = [IO.Path]::Combine($PSCommandPath, '7z.exe')
}
else {
	# fail (7z not found)
	$Script:total_errors++
	# exit
}

# datetime now
$DateTimeNow = [DateTime]::Now

# datetime prefix
switch ($prefix) {
	'iso' {
		$dt_prefix = $DateTimeNow.ToString('yyyy-MM-ddTHHmmss')
	}
	'parity' {
		if ($([DateTime]::Now.Date.Day % 2)) {
			$dt_prefix = '1'
		} else {
			$dt_prefix = '2'
		}
	}
	Default {
		$dt_prefix = ''
	}
}

# archive_name
if ($dt_prefix.Length -gt 0) {
	$arch_path = [IO.Path]::Combine($dst, $name + '_' + $dt_prefix)
} else {
	$arch_path = [IO.Path]::Combine($dst, $name)
}

New-Variable -Name ArgumentList -Force -ErrorAction SilentlyContinue
Join-Rows ([ref]$ArgumentList) 'a'
Join-Rows ([ref]$ArgumentList) '-y' -space
Join-Rows ([ref]$ArgumentList) '-bd' -space
Join-Rows ([ref]$ArgumentList) $arch_path -space
Join-Rows ([ref]$ArgumentList) $src -space

[DateTime]::Now.ToString('u').Replace('Z',''), (Get-Culture).TextInfo.ToTitleCase([IO.Path]::GetFileNameWithoutExtension($exe)), 'running' | Join-Words | Logging -var ([ref]$log) -file $logfile
'Argument:', $ArgumentList | Join-Words | Logging -var ([ref]$log) -file $logfile

$Global:result = Invoke-Process -FileName $exe -Arguments $ArgumentList

if (! [string]::IsNullOrWhiteSpace($result.Elapsed)) {
	'Elapsed:', $result.Elapsed.ToString() | Join-Words | Logging -var ([ref]$log) -file $logfile
}

if (! [string]::IsNullOrWhiteSpace($result.stdout)) {
	'stdout:' | Join-Words | Logging -var ([ref]$log) -file $logfile
	$result.stdout.Trim() | Join-Words | Logging -var ([ref]$log) -file $logfile
}

if (! [string]::IsNullOrWhiteSpace($result.stderr)) {
	'stderr:' | Join-Words | Logging -var ([ref]$log) -file $logfile
	$result.stderr.Trim() | Join-Words | Logging -var ([ref]$log) -file $logfile
}

if (! [string]::IsNullOrEmpty($result.Catch)) {
	# fail (catch in execite process)
	$Script:total_errors++
	'Exception:', $result.Catch | Join-Words| Logging -var ([ref]$log) -file $logfile
	
}

if ([string]::IsNullOrEmpty($result.ExitCode)) {
	# fail (exit code not existing)
	$Script:total_errors++
} else {
	New-Variable -Name Text -Force -ErrorAction SilentlyContinue
	Join-Rows ([ref]$Text) 'ExitCode:'
	Join-Rows ([ref]$Text) $result.ExitCode -space
	if (! [string]::IsNullOrEmpty((Get-ExitCodeDescription7z -exitcode $result.ExitCode))) {
		Join-Rows ([ref]$Text) ('(' +  (Get-ExitCodeDescription7z -exitcode $result.ExitCode) + ')') -space
	}
	$Text | Logging -var ([ref]$log) -file $logfile
	if ($result.ExitCode -gt 1) {
		# fail (bad return code)
		$Script:total_errors++
	}
}

# Invoke-Process -FileName $exe -Arguments '-h'
# $fileName = "path_to_file"; $file = [System.io.File]::Open($fileName, 'Open', 'Read', 'None'); Write-Host "Press any key to continue ..."; $null = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown"); $file.Close()
